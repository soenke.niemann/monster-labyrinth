JAVAC=javac
sources = $(wildcard *.java)
classes = $(sources:.java=.class)

default:	$(classes)

all:	$(classes)

%: %.java
		java -jar checkstyle.jar -c checks_own6.12.1.xml $@.java

clean:
	rm -f *.class

%.class : %.java
	$(JAVAC) $<
