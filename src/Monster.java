// BeginClassMonster
/**
* Die Klasse Monster verwaltet/verarbeitet die Stats/Werte des Monsters.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Monster extends Character {

    //------------------------GETTER/SETTER------------------------

    /**
     * Diese Methode macht NICHTS, für den Fall, dass der Eingabewert '-1' beträgt,
     * für den Fall, dass der Eingabewert nicht '-1' beträgt, wird 'ganz normal'
     * laut Characterklasse der ATK-Wert des Monsters gesetzt.
     */
    @Override public void setATK(int inputATK) {
        if (inputATK == -1) {
        } else {
            super.setATK(inputATK);
        }
    }

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Konstruktor der Klasse Monster
     */
    public Monster() {
    }

    /**
     * Konstruktor der Klasse Monster
     * @param inputPlayer Werte des Players
     */
    public Monster(Player inputPlayer) {
        this.hp = (int) (inputPlayer.getHP() * 1.25);
        this.atk = inputPlayer.getATK() / 2;
        this.hit = 50;
        this.alive = true;
        this.name = "StanniMonster";
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Methode die einen String mit den Special Features eines Monsters am Anfang des Spiels ausgibt.
     * @return String Ausgabe
     */
    public String printSpecialFeatureStart() {
        String ausgabe = "";
        return ausgabe;
    }

    /**
     * Methode die einen String mit den Special Features eines Monsters jede Runde ausgibt.
     * @return String Ausgabe
     * @param inputPlayer Benutzt den Namen des Players
     */
    public String printSpecialFeatureAttack(Player inputPlayer) {
        String ausgabe = "--> " + name + " attacks " + inputPlayer.getName() + " with " + atk + " ATK!%n";
        return ausgabe;
    }

}
// EndClassMonster
