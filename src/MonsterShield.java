// BeginClassMonsterShield
/**
* Die Klasse Monster verwaltet/verarbeitet die Stats/Werte des Monsters.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class MonsterShield extends Monster {

    //------------------------KLASSENVARIABLEN------------------------

    /** Integer-Wert mit dem maxShield-Werts des MonsterShield */
    private int maxShield = 80;
    /** Integer-Wert mit dem Shield-Werts des MonsterShield */
    private int shield = (int) (maxShield * Math.random());

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Diese Methode weißt dem Monster seine Werte anhand des Players zu
     * @param inputPlayer Werte des Players
     */
    public MonsterShield(Player inputPlayer) {
        this.hp = (int) (inputPlayer.getHP());
        this.atk = (int) (inputPlayer.getATK() / 3);
        this.hit = 50;
        this.alive = true;
        this.name = "MonsterShield";
    }

    //------------------------GETTER/SETTER------------------------

    //------------------------KLASSENMETHODEN------------------------

    @Override public String printSpecialFeatureStart() {
        String ausgabe = "";
        ausgabe = name + " has " +  shield + " Shield!";
        return ausgabe;
    }

    @Override public String takeDamage(int inputDamage) {
        String ausgabe = "";
        int absorbedDamage;
        int damageTaken;

        if (shield <= 4) {
            shield = 0;
        }

        if (inputDamage >= shield) {
            absorbedDamage = shield;
            damageTaken = inputDamage - absorbedDamage;
            hp = hp - damageTaken;
            shield = shield - (int) (0.5 * absorbedDamage);
        } else {
            absorbedDamage = inputDamage;
            damageTaken = inputDamage - absorbedDamage;
            hp = hp - damageTaken;
            shield = shield - (int) (inputDamage * 0.5);
        }

        ausgabe = "    " + name + "'s shield absorbs " + absorbedDamage;
        ausgabe = ausgabe + " ATK%n    " + name + " HP suffers from " + damageTaken + " ATK%n";
        return ausgabe;
    }
}
// EndClassMonsterShield
