// BeginClasswell
/**
* Diese Klasse beinhaltet alle Methoden für das Tile-Well
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Well extends Tile implements PlayerAction {
    {
        walkable = true;
        tileSymbol = "O";                                       //OHNE FARBE
        tileSymbolColoured = "\033[1;34m" + "O" + "\033[0m";    //MIT FARBE
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * Wenn man also auf das Well-Tile kommt bekommt man Leben dazu.
     * @param player Werte des Players
     */
    public void startAction(Player player) {
        System.out.println("--> " + player.getName() + " drinks the healthy fluid of the Healing Well!");
        //benutzt Potion und gibt den Ausgabestring aus und printet ihn
        System.out.printf(player.useHealingWell());

    }
}
// EndClassWell
