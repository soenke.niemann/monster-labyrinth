import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

//BeginClassMap
/**
* Zusammenfassung der Spielfelder
* Laden der Karte
* Zeichnen der Karte
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Map {

    //-----------------------KLASSENVARIABLEN-----------------------

    /** Integer mit der X Koordinate des Players im Array */
    protected int playerXCoordinate;
    /** Integer mit der Y Koordinate des Players im Array */
    protected int playerYCoordinate;
    /** Player Objekt */
    protected Player player;
    /** String für den Dateipfad der Map.txt */
    private String path;
    /** Tile-Array, welches die Map bildet */
    protected Tile[][] map;
    /** Integer mit der Anzahl der Zeilen der Textdatei-Map */
    protected int height = 0;
    /** Integer mit der Anzahl der Spalten der Textdatei-Map */
    protected int width = 0;
    /** Legt fest, ob Steuerzeichen für Farben auf der Map verwendet werden sollen*/
    protected boolean colouredMap;

    //------------------------KONSTRUKTOR------------------------

    /**
     * Konstruktor der Klasse Map
     * @param path Der Pfad der Map
     * @param player Der Player
     */
    public Map(String path, Player player) {
        this.path = path;
        this.player = player;
    }

    /**
     * abstrakter Konstruktor der Klasse Map
     */
    public Map() {

    }

    //------------------------GETTER/SETTER------------------------

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Diese Methode liest eine gespeicherte Karte aus der Textdatei ein und erstellt das Map Array.
     * @throws IOException s
     */
    public void loadSavedMap() throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(path));

        //---Abmessungen und Anzahl der verschiedenen Tiles auslesen--------
        String line = null;
        int leseZeile = 0;
        int goalCount = 0;
        int playerTileCount = 0;

        while ((line = br.readLine()) != null) {

            for (int i = 0; i < line.length(); i++) {
                switch (line.charAt(i)) {
                    case 'P': playerTileCount++;
                        break;
                    case 'G': goalCount++;
                        break;
                    default:
                        break;
                }
            }

            height++;
            width = line.length();
        }

        //FALLS MEHR ALS EIN P ODER WENIGER ALS EIN G:
        if (playerTileCount > 1 || goalCount < 1) {
            System.out.println("Error! Too many Players or Goals on the map!");
            System.exit(0);
        }

        map = new Tile[height][width];

        //------------------------Spielfeld einlesen, Tiles Array füllen
        line = null;
        leseZeile = 0;

        BufferedReader br2 = Files.newBufferedReader(Paths.get(path));

        while ((line = br2.readLine()) != null) {
            for (int i = 0; i < line.length(); i++) {
                switch (line.charAt(i)) {
                    case '#':
                        map[leseZeile][i] = new Wall();
                        break;
                    case 'P':
                        map[leseZeile][i] = new PlayerTile();
                        playerXCoordinate = i;
                        playerYCoordinate = leseZeile;
                        break;
                    case 'B':
                        map[leseZeile][i] = new Battle();
                        break;
                    case 'O':
                        map[leseZeile][i] = new Well();
                        break;
                    case 'G':
                        map[leseZeile][i] = new Goal();
                        break;
                    case ' ':
                        map[leseZeile][i] = new Empty();
                        break;
                    default:
                        break;

                }
            }
            leseZeile++;
        }
    }

    /**
     * Printet die Map
     */
    public void printMap() {
        System.out.println();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.printf(map[i][j].toString(colouredMap));
                System.out.printf(" ");
            }
            switch (i) {
                case 0: System.out.printf("  |S| " + player.getName() + "'s HP: " + player.getHP());
                    break;
                case 1: System.out.printf("  |T| " + player.getName() + "'s ATK: " + player.getATK());
                    break;
                case 2: System.out.printf("  |A| " + player.getName() + "'s HIT: " + player.getHIT());
                    break;
                case 3: System.out.printf("  |T| ");
                    break;
                case 4: System.out.printf("  |S| " + "Defeated Monsters: " + player.getDefeatedMonsters());
                    break;
                default:
                    break;
            }
            System.out.printf("%n");
        }
        System.out.println("");
    }


    /**
     * Diese Methode gibt das Spielfeld aus und ruft die Bewegung und Aktionen auf 
     */
    public void start() {

        //KARTE PRINTEN
        printMap();

        //BEWEGUNGSABFRAGE
        System.out.println("--> Where's your adventure going?");
        System.out.println("    (hit key: W=up, A=left, S=down, D=right)");

        //SCANNER
        java.util.Scanner scannerDirection = new java.util.Scanner(System.in);
        String readInput = "";
        do {
            if (scannerDirection.hasNext("W")) {
                readInput = "W";
            }
            if (scannerDirection.hasNext("A")) {
                readInput = "A";
            }
            if (scannerDirection.hasNext("S")) {
                readInput = "S";
            }
            if (scannerDirection.hasNext("D")) {
                readInput = "D";
            }
            scannerDirection.next();
        } while ((!readInput.equals("W")) & (!readInput.equals("S")) & (!readInput.equals("A"))
                  & (!readInput.equals("D")));

        move(readInput);
    }

    /**
     * Diese Methode bewegt den Spieler und startet die Action auf dem neuen Tile.
     * @param richtung Die Richtung in die der Player geht.
     */
    public void move(String richtung) {
        boolean changedPosition = false;
        int oldPlayerYCoordinate = playerYCoordinate;
        int oldPlayerXCoordinate = playerXCoordinate;

        switch (richtung) {
            case "W":
                if (map[playerYCoordinate - 1][playerXCoordinate].isWalkable()) {
                    playerYCoordinate = playerYCoordinate - 1;
                    changedPosition = true;
                }
                break;
            case "A":
                if (map[playerYCoordinate][playerXCoordinate - 1].isWalkable()) {
                    playerXCoordinate = playerXCoordinate - 1;
                    changedPosition = true;
                }
                break;
            case "S":
                if (map[playerYCoordinate + 1][playerXCoordinate].isWalkable()) {
                    playerYCoordinate = playerYCoordinate + 1;
                    changedPosition = true;
                }
                break;
            case "D":
                if (map[playerYCoordinate][playerXCoordinate + 1].isWalkable()) {
                    playerXCoordinate = playerXCoordinate + 1;
                    changedPosition = true;
                }
                break;
            default:
                break;
        }

        if (changedPosition) {
            map[oldPlayerYCoordinate][oldPlayerXCoordinate] = new Empty();
            map[playerYCoordinate][playerXCoordinate].startAction(player);

            map[playerYCoordinate][playerXCoordinate] = new PlayerTile();
        } else {
            System.out.println("--> You can't walk though walls!");
            System.out.println();
        }

        start();
    }
}
// EndClassMap
