// BeginClassPlayer
/**
* Die Klasse Player verwaltet/verarbeitet die Stats/Werte des Players.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Player extends Character {

    //------------------------KLASSENVARIABLEN------------------------

    /** Zähler für getötete Monster */
    private int defeatedMonsters = 0;

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Konstruktor der Klasse Player
     * @param hp Integer-Wert mit dem HP-Wert des Players
     * @param atk Integer-Wert mit dem ATK-Wert des Players
     * @param hit Integer-Wert mit dem HIT-Wert des Players
     * @param name String mit dem Namen des Players
     */
    public Player(int hp, int atk, int hit, String name) {
        super(hp, atk, hit, name);
    }

    //------------------------GETTER/SETTER------------------------

    /**
     * Getter for {@link #defeatedMonsters} Gibt den integer-Wert des besiegte Monsterzählers aus
     * @return {@link #defeatedMonsters}
     */
    public int getDefeatedMonsters() {
        return defeatedMonsters;
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Der Spieler benutzt einen Heiltrank um seine HP aufzufüllen
     * @return String Ausgabe
     */
    public String useHealingWell() {
        String ausgabe = "";
        int hpBefore = hp;
        hp = hp + 50;
        if (hp > maxHP) {
            hp = maxHP;
        }
        int hpAfter = hp;
        int hpGain = hpAfter - hpBefore;

        ausgabe = "--> " + name + " increases his HP by " + hpGain + " HP to " + hp + " HP %n";
        return ausgabe;
    }

    /**
     * Zählt den Counter der besiegten Monster hoch
     */
    public void increaseDefeatedMonsters() {
        defeatedMonsters++;
    }

}
// EndClassPlayer
