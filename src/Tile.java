//BeginClassTile
/**
* Abstrakte Oberklasse für alle Spielfeld
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Tile implements PlayerAction {

    //------------------------KLASSENVARIABLEN------------------------

    /** String mit dem Tile-Symbol */
    protected String tileSymbol = "";
    /** String mit dem farbigen Tile-Symbol */
    protected String tileSymbolColoured = "";
    /** Boolean-Variable ob man das Tile betreten kann oder nicht */
    protected boolean walkable = true;

    //------------------------GETTER/SETTER------------------------

    /**
     * Getter for {@link #walkable}
     * @return {@link #walkable}
     */
    public boolean isWalkable() {
        return walkable;
    }

    /**
     * Getts the Tile Symbol for {@link #tileSymbol}
     * @param coloured Farbe für Symbole an oder aus
     * @return {@link #tileSymbol}
     */
    public String toString(boolean coloured) {
        if (coloured) {
            return tileSymbolColoured;
        } else {
            return tileSymbol;
        }
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * @param player Werte des Players
     */
    public void startAction(Player player) {

    }


}
// EndClassTile
