// BeginClassPlayerTile
/**
* Diese Klasse beinhaltet alle Methoden für das Tile-Player.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/
public class PlayerTile extends Empty {

    {
        tileSymbol = "P";                                       //OHNE FARBE
        tileSymbolColoured = "\033[1;32m" + "P" + "\033[0m";    //MIT FARBE
    }
}

// EndClassPlayerTile
