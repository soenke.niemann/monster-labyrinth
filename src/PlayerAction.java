// BeginInterfacePlayerAction
/**
* Diese Klasse beinhaltet das Gameplay/die Methoden für den Ablauf des Spieles.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public interface PlayerAction {

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * @param player Werte des Players
     */
    public void startAction(Player player);
}

// EndClassPlayerAction
