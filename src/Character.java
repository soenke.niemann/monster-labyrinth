// BeginClassCharacter
/**
* Die Klasse Character verwaltet/verarbeitet die Stats/Werte der Character.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Character {

    //------------------------KLASSENVARIABELN------------------------

    /** Integer-Wert mit dem HP-Wert des Character */
    protected int hp;
    /** Integer-Wert mit dem ATK-Wert des Character */
    protected int atk;
    /** Integer-Wert mit dem HIT-Wert des Character */
    protected int hit;
    /** Integer-Wert mit dem maxHP-Wert des Character */
    protected int maxHP;
    /** Character lebt ja/nein */
    protected boolean alive;
    /** String mit dem Namen des Characters */
    protected String name;

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Diese Methode weißt dem Charakter seine Werte zu
     * @param hp Integer-Wert mit dem HP-Wert des Character
     * @param atk Integer-Wert mit dem ATK-Wert des Character
     * @param hit Integer-Wert mit dem maxHP-Wert des Character
     * @param name String mit dem Namen des Characters
     */
    public Character(int hp, int atk, int hit, String name) {
        this.hp = hp;
        this.atk = atk;
        this.hit = hit;
        this.maxHP = hp;
        if (hp > 0) {
            this.alive = true;
        } else {
            this.alive = false;
        }

        this.name = name;
    }

    /**
     * Superkonstruktor für Monster() bei Battle.java
     */
    public Character() {
    }

    //------------------------GETTER/SETTER------------------------

    /**
     * Getter for {@link #hp}
     * @return {@link #hp}
     */
    public int getHP() {
        return hp;
    }

    /**
     * Getter for {@link #atk}
     * @return {@link #atk}
     */
    public int getATK() {
        return atk;
    }

    /**
     * Setter for {@link #atk}
     * Setzt ATK-Wert des Characters
     * @param inputATK the {@link #atk} to set
     */
    public void setATK(int inputATK) {
        atk = inputATK;
    }

    /**
     * Getter for {@link #hit}
     * @return {@link #hit}
     */
    public int getHIT() {
        return hit;
    }

    /**
     * Aktualisiert und gibt den alive Wert des Characters zurück
     * @return alive-Wert(boolean) des Characters
     */
    public boolean isAlive() {
        if (hp < 1) {
            alive = false;
        } else {
            alive = true;
        }
        return alive;
    }

    /**
     * Getter for {@link #name}
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * @return String des HP-Wertes des Characters mit ausgabesatz
     */
    public String toString() {
        String ausgabe = getName() + " has " + hp + " HP";
        return ausgabe;
    }

    /**
     * Ermittelt, ob der Character das Ziel trifft
     * @return Angriffswert des Characters, liefert -1 falls 'miss'
     */
    public int checkIfHitting() {
        int isHitting = 0;
        int rndm = (int) (100 * Math.random());
        if (rndm > hit) {
            isHitting = -1;
        } else {
            isHitting = hit;
        }
        return isHitting;
    }

    /**
     * Der Character nimmt Schaden
     * @param inputDamage ist der Betrag des Schadens, den der Character erleidet
     * @return String ausgabe
     */
    public String takeDamage(int inputDamage) {
        hp = hp - inputDamage;
        String ausgabe = "    " + getName() + " suffers from " + inputDamage + " ATK%n";

        return ausgabe;
    }

}
// EndClassCharacter
