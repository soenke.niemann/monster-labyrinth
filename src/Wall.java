// BeginClassWall
/**
* Diese Klasse beinhaltet alle Methoden für das Tile-Wall
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/


public class Wall extends Tile {
    {
        walkable = false;
        tileSymbol = "#";                                   //OHNE FARBE
        tileSymbolColoured = "\033[1m" + "#" + "\033[0m";   //MIT FARBE
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * Wenn man also auf das Wall-Tile kommt passiert nichts.
     * @param player Werte des Players
     */
    public void startAction(Player player) {

    }

}
// EndClassWall
