// BeginClassGoal
/**
* Diese Klasse beinhaltet alle Methoden für das Tile-Goal.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Goal extends Tile implements PlayerAction {

    {
        walkable = true;
        tileSymbol = "G";                                       //OHNE FARBE
        tileSymbolColoured = "\033[1;32m" + "G" + "\033[0m";    //MIT FARBE
    }

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * Wenn der Player also auf das Goal-Tile kommt wird das Spiel beendet und der Player hat gewonnen.
     * @param player Werte des Players
     */
    public void startAction(Player player) {
        System.out.println("-------------------------------------------------");
        System.out.println("--> You've reached your Goal!");
        System.out.println("--> ***YOU WIN***");
        System.out.println("-------------------------------------------------");
        System.exit(0);
    }
}
// EndClassGoal
