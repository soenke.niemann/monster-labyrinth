// BeginClassBattle
/**
* Diese Klasse bildet die Spielfelder "Battle" und startet die Kämpfe
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Battle extends Tile implements PlayerAction {
    {
        walkable = true;
        tileSymbol = "B";                                     //OHNE FARBE
        tileSymbolColoured = "\033[5;31m" + "B" + "\033[0m";  //MIT FARBE
    }

    /**
     * Diese Methode führt alle Actions aus, die passieren wenn der Player auf ein
     * "BATTLE" Feld kommt.
     * @param player das Player-Objekt, aus dem die Werte gelesen werden
     */
    public void startAction(Player player) {
        Fight fight1 = new Fight();                             //Erstelle neues Gameplay/Fight-Objekt

        //Weise dem allgemeinen Monster monster1 die zugehörigen Werte zu, die vom Player player1 abhängen
        Monster monster1 = new Monster();

        //------------------------RANDOM MONSTERAUSWAHL------------------------

        int rndm = (int) (3 * Math.random());
        switch (rndm) {
            case 0:
                monster1 = new Monster(player);
                break;
            case 1:
                monster1 = new MonsterShield(player);
                break;
            case 2:
                monster1 = new MonsterStriker(player);
                break;
            default:
                monster1 = new Monster(player);
                break;
        }

        //Ruft die StartSequenz für ein Battle auf.
        fight1.announceBattle(player, monster1);

        //Wenn Spieler und Monster am Leben sind, wird eine neue Runde gestartet.
        while (player.isAlive() && monster1.isAlive()) {
            fight1.nextBattleRound(player, monster1);
        }

        //Beende das Spiel sofort, falls Spieler tot ist
        if (!player.isAlive()) {
            System.out.println();
            System.out.println("-------------------------------------------------");
            System.out.println("MONSTER DEFEATED YOU!");
            System.out.println("***GAME OVER***");
            System.out.println("-------------------------------------------------");

            System.exit(0);
        }

        //Gibt aus, dass das Monster tod ist
        if (!monster1.isAlive()) {
            System.out.println();
            System.out.println("-------------------------------------------------");
            System.out.println("***" + monster1.getName() +  " DEFEATED***");
            System.out.println("-------------------------------------------------");

            //Counter für besiegte Monster wir erhöht
            player.increaseDefeatedMonsters();
        }
    }
}
// EndClassBattle
