// BeginClassFight
/**
* Diese Klasse beinhaltet die verschiedenen Fight-Methoden (die dann z.B. ausgeführt werden,
* wenn der Player auf ein Battle-Tile kommt) um den Kampf auszuführen
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Fight {

    //-----------------------KLASSENVARIABLEN-----------------------

    /** Rundenzähler */
    private int round;

    //------------------------GETTER/SETTER------------------------

    /**
     * Getter for {@link #round} Gibt den integer-Wert des Rundenzählers aus
     * @return {@link #round}
     */
    public int getRound() {
        return round;
    }

    /**
     * Setter for {@link #round} Setzt HP-Wert des Characters
     * @param inputRound the {@link #round} to set
     */
    public void setRound(int inputRound) {
        round = inputRound;
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Diese Methode gibt für den Start des Spieles die Stats des Players und des Monsters aus.
     * @param inputPlayer das Player-Objekt, aus dem die Werte gelesen werden
     * @param inputMonster das Monster-Objekt, aus dem die Werte gelesen werden
     */
    public void announceBattle(Player inputPlayer, Monster inputMonster) {
        round = 0;

        System.out.println("-------------------------------------------------");
        System.out.println("***BATTLE STARTED***");
        System.out.println("-------------------------------------------------");
        System.out.println("");
        System.out.println("--> A " + inputMonster.getName() + " was summoned!");
        System.out.println(inputPlayer.toString());                             //HP-Ausgabe
        System.out.println(inputPlayer.getName() + " has " + inputPlayer.getATK() + " ATK");
        System.out.println(inputPlayer.getName() + " has " + inputPlayer.getHIT() + " HIT");
        System.out.printf("%n" + inputMonster.toString() + "%n");               //HP-Ausgabe
        System.out.println(inputMonster.getName() + " has " + inputMonster.getATK() + " ATK");
        System.out.println(inputMonster.getName() + " has " + inputMonster.getHIT() + " HIT");
        //SHIELD, STRIKER Ausgabe der Info zu den Special Features in der Konsole
        System.out.println(inputMonster.printSpecialFeatureStart());
    }

    /**
     * Diese Methode gibt für jede Spielrunde die Stats des Players und des Monsters aus.
     * @param inputPlayer das Player-Objekt, aus dem die Werte gelesen werden
     * @param inputMonster das Monster-Objekt, aus dem die Werte gelesen werden
     */
    public void nextBattleRound(Player inputPlayer, Monster inputMonster) {
        //kurze Pause von 750 ms, damit der Kampf optisch besser aussieht und man den Kampf mitverfolgen kann.
        try {
            Thread.sleep(750);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        round++;
        System.out.println("_________________________________________________");
        System.out.println("");
        System.out.println("ROUND #" + round);
        System.out.println(inputPlayer.toString());                             //HP-Ausgabe
        System.out.println(inputMonster.toString());                            //HP-Ausgabe
        System.out.println(inputMonster.printSpecialFeatureStart());            //Monster mit Special Feature Ausgabe

        fight(inputPlayer, inputMonster);
    }

    /**
     * Diese Methode simuliert den Kampf des Players und des Monsters.
     * @param inputPlayer Der Player, der kämpft
     * @param inputMonster Das Monster, das kämpft
     */
    public void fight(Player inputPlayer, Monster inputMonster) {
        if (inputPlayer.isAlive() && inputMonster.isAlive()) {
            //PLAYER-ANGRIFF
            if (inputPlayer.checkIfHitting() >= 0) {
                System.out.printf("--> " + inputPlayer.getName() + " attacks ");
                System.out.printf(inputMonster.getName() + " with " + inputPlayer.getATK() + " ATK!%n");
                System.out.printf(inputMonster.takeDamage(inputPlayer.getATK()));
            } else {
                System.out.println("--> " + inputPlayer.getName() + " misses!");
            }

            //MONSTER-ANGRIFF
            if (inputMonster.isAlive()) {
                if (inputMonster.checkIfHitting() >= 0) {
                    //Aufruf der setATK-Methode für Monster mit speziellen Bedingungen der ATK
                    //wie zum Beispiel Monster Striker, tut bei normalen NICHTS
                    inputMonster.setATK(-1);
                    //Ausgabe für Special Features des Monster-Angriffs
                    System.out.printf(inputMonster.printSpecialFeatureAttack(inputPlayer));
                    //Player nimmt Schaden den das Monster austeilt
                    System.out.printf(inputPlayer.takeDamage(inputMonster.getATK()));
                } else {
                    System.out.println("--> " + inputMonster.getName() + " misses!");
                }
            }
        }
    }
}
// EndClassFight
