// BeginClassMonster
/**
* Die Klasse Monster verwaltet/verarbeitet die Stats/Werte des Monsters.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class MonsterStriker extends Monster {

    //------------------------KLASSENVARIABELN------------------------

    /** Integer-Wert mit der Chance zu einen kritischen Treffers in Prozent des MonsterStriker */
    private int chanceToCritHit = 10;
    /** Integer-Wert mit der maximalen Anzahl der möglichen Kritischen Treffern */
    private int maxAmountCritHits = 2;
    /** Boolean-Variable die anzeigt ob ein kritischer Treffer ausgeführt wird oder nicht */
    private boolean strikesCritHit = false;
    /** Integer-Wert dem specialATK-Wert des MonsterStriker */
    private int specialATK;

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Konstruktor des Monster Striker
     * @param inputPlayer Werte des Players
     */
    public MonsterStriker(Player inputPlayer) {
        this.hp = (int) (0.8 * inputPlayer.getHP());
        this.atk = (int) (inputPlayer.getATK() * 0.6);
        this.specialATK = this.atk;
        this.hit = 50;
        this.alive = true;
        this.name = "MonsterStriker";
    }

    //------------------------GETTER/SETTER------------------------

    /**
    * Diese Methode gibt den speziellen ATK Wert aus
    * Durch Crit Hits ist gilt: atk != specialATK
    * @return spezielle ATK (Critical ATK)
    */
    @Override public int getATK() {
        return specialATK;
    }

    //------------------------KLASSENMETHODEN------------------------

    /**
    * Diese Methode legt unter Zufall fest, ob das Monster einen Critical Hit ausführt
    * (3-facher Schaden des normalen atk-Wertes)
    * Falls bereits zu viele CritHits ausgeführt wurden (maxAmountCritHits) oder der Zufall es nicht wollte,
    * wird keiner ausgeführt sondern der normale ATK-Wert in der Klassenvariable specialATK gesetzt.
    */
    @Override public void setATK(int inputATK) {
        strikesCritHit = false;
        int rndm = (int) (100 * Math.random());

        if ((maxAmountCritHits > 0) && (rndm < chanceToCritHit)) {
            maxAmountCritHits = maxAmountCritHits - 1;
            specialATK = 3 * atk;
            strikesCritHit = true;
        } else {
            specialATK = atk;
        }
    }

    @Override public String printSpecialFeatureStart() {
        String ausgabe = "";
        ausgabe = name + " has " + maxAmountCritHits + " CritAttacks and a chance of ";
        ausgabe = ausgabe + chanceToCritHit + "% to do a crithit.";
        return ausgabe;
    }

    @Override public String printSpecialFeatureAttack(Player inputPlayer) {
        String ausgabe = "";
        if (strikesCritHit) {
            ausgabe = "--> " + name + " performs critical Hit against ";
            ausgabe = ausgabe + inputPlayer.getName() + " with " + specialATK + " ATK!%n";
        } else {
            ausgabe = "--> " + name + " attacks " + inputPlayer.getName() + " with " + specialATK + " ATK!%n";
        }

        return ausgabe;
    }
}
//EndClassMonsterCritical
