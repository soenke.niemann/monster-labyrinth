// BeginClassEmpty
/**
* Diese Klasse beinhaltet alle Methoden für das Tile-Empty.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Empty extends Tile implements PlayerAction {

    {
        walkable = true;
        tileSymbol = " ";
        tileSymbolColoured = " ";          //MIT FARBE
    }

    /**
     * Diese Methode beinhaltet alle Actions die ausgeführt werden, wenn der Player auf dieses Tile kommt.
     * Bei dem Empty-Tile passiert dementsprechend nichts.
     * @param player Werte des Players
     */
    public void startAction(Player player) {

    }
}
// EndClassEmpty
