import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

//BeginClassGeneratedMap
/**
* Beinhaltet alle Methoden um die Karte zu generieren.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class GeneratedMap extends Map {

    //-----------------------KLASSENVARIABLEN-----------------------

    /** true falls die Rekursion das Goal erreicht */
    private boolean goalReached = false;
    /** Array mit zufälliger Richtungsreihenfolge für Rekurion */
    private int[] randomOrder = new int[4];
    /** Integer mit der X-Koordinate für den nächsten Rekursionsschritt */
    private int newCoordinateX;
    /** Integer mit der Y-Koordinate für den nächsten Rekursionsschritt */
    private int newCoordinateY;

    //------------------------KONSTRUKTOREN------------------------

    /**
     * Konstruktor der Klasse GeneratedMap
     * @param width Breite
     * @param height Höhe
     * @param player Player
     * @param colouredMap Farbe der Symbole an oder aus
     */
    public GeneratedMap(int width, int height, Player player, boolean colouredMap) {
        while (width <= 3 || (width % 2 == 0)) {
            width++;
        }
        this.width = width;
        while (height <= 3 || (height % 2 == 0)) {
            height++;
        }
        this.height = height;
        this.player = player;
        this.colouredMap = colouredMap;
    }

    //------------------------GETTER/SETTER------------------------

    //------------------------KLASSENMETHODEN------------------------

    /**
     * Erstellt das Tile Array das mit null gefüllt wird (besser gesagt nciht gefüllt wird)
     */
    public void createNullMap() {
        map = new Tile[height][width];
    }

    /**
     * Erzeugt die Außenwände sowie die Raumbegrenzungen
     */
    public void createOuterWalls() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //Außenwand
                if (i == 0 || i == (width - 1) || j == 0 || j == (height - 1)) {
                    map[j][i] = new Wall();
                }
                //Walls wenn beide Koordinaten gerade sind, damit Wege und Räume nur 1 breit sind
                if ((j % 2 == 0) && (i % 2 == 0)) {
                    map[j][i] = new Wall();
                }
            }
        }
    }

    /**
     * Platziert den Player und das Goal auf der Map
     */
    public void plantPlayerAndGoal() {
        int randomPlayerX;
        int randomPlayerY;
        int randomGoalX;
        int randomGoalY;
        double distanceGoalToPlayer;
        double minimalDistance;             //in Prozent des Durchschnitts von Höhe und Weite der Map

        //Legt die Minimal Distance fest
        if (width < 10 | height < 10) {
            minimalDistance = 0.5;
        } else {
            minimalDistance = 0.9;
        }

        do {
            //Erzeuge random Koordinatenpaare für Player und Goal
            int min = 1;
            int max = width - 1;
            randomPlayerX = ThreadLocalRandom.current().nextInt(min, max); //INKLUSIVES min, EXKLUSIVES max!!

            max = height - 1;
            randomPlayerY = ThreadLocalRandom.current().nextInt(min, max);

            max = width - 1;
            randomGoalX = ThreadLocalRandom.current().nextInt(min, max);

            max = height - 1;
            randomGoalY = ThreadLocalRandom.current().nextInt(min, max);

            //Berechnung des Abstandes von Player zu Goal
            distanceGoalToPlayer = (Math.sqrt(
            Math.pow((double) (randomPlayerX) - (double) (randomGoalX), (double) 2)
            +  Math.pow((double) (randomPlayerY) - (double) (randomGoalY), (double) 2)));

        } while (
            (distanceGoalToPlayer <= ((width + height) / 2 * minimalDistance)) //Minimalabstand zwischen P und G
            | (map[randomPlayerY][randomPlayerX] instanceof Wall)              //Player darf nicht auf Wall
            | (map[randomGoalY][randomGoalX] instanceof Wall)                  //Goal darf nicht auf Wall
        );

        //Setze Player und Goal Tile final auf die Karte, definiere Playerkoordinaten
        map[randomGoalY][randomGoalX] = new Goal();
        map[randomPlayerY][randomPlayerX] = new PlayerTile();
        playerXCoordinate = randomPlayerX;
        playerYCoordinate = randomPlayerY;
    }

    /**
     * Rekursive Map Generator Methode
     * @param coordinateX X-Koordinate
     * @param coordinateY Y-Koodrinate
     * @return ob die Rekursion weitergeht oder fertig ist
     */
    public boolean generateRecursiveMap(int coordinateX, int coordinateY) {

    //Falls das Goal bereits erreicht wurde, wird Rekursion komplett abgebrochen
        if (!goalReached) {

            //Bewegungsrichtungen werden definiert als  0=rechts, 1=oben, 2=links, 3=unten
            //Erzeuge ein int Array mit 4 Einträgen, das die Zahlen von 0 bis 4 in zufälliger Reihenfolge bringt
            Random randomGenerator = new Random();
            for (int i = 0; i < 4; i++) {
                int r = randomGenerator.nextInt(i + 1);
                randomOrder[i] = randomOrder[r];
                randomOrder[r] = i;
            }

            //Gehe in random Reihenfolge (durch das eben erzeugte Array) einmal in alle 4 Richtungen
            for (int i = 0; i < 4; i++) {
                newCoordinateX = coordinateX;
                newCoordinateY = coordinateY;

                //Schreiben der neuen Koordinate in welche Richtung es geht
                switch (randomOrder[i]) {
                    case 0:   newCoordinateX++;
                        break;
                    case 1:   newCoordinateY--;
                        break;
                    case 2:   newCoordinateX--;
                        break;
                    case 3:   newCoordinateY++;
                        break;
                    default:
                        break;
                }

                //FALLS ER BEIM GOAL ANKOMMT
                if (map[newCoordinateY][newCoordinateX] instanceof Goal) {
                    goalReached = true;   //Führt dann beim nächsten Ausführen zum Abbruch der Rekursion
                    return false;         //Bricht aktuellen Aufruf ab
                }

                //Wenn Feld auf dem nächste Rekursion ausgeführt weden soll FREI ist
                if (!(map[newCoordinateY][newCoordinateX] instanceof Battle
                    | map[newCoordinateY][newCoordinateX] instanceof Empty
                    | map[newCoordinateY][newCoordinateX] instanceof PlayerTile
                    | map[newCoordinateY][newCoordinateX] instanceof Wall
                    | map[newCoordinateY][newCoordinateX] instanceof Well
                    | map[newCoordinateY][newCoordinateX] instanceof Goal
                    )) {

                    //Fülle das Feld random mit Tiles
                    int randomTile = ThreadLocalRandom.current().nextInt(1, 101);

                    //Erzeuge Empty Tile
                    if (randomTile <= 70) {
                        map[newCoordinateY][newCoordinateX] = new Empty();
                    }
                    //Erzeuge Battle Tile
                    if (70 < randomTile && randomTile <= 90) {
                        map[newCoordinateY][newCoordinateX] = new Battle();
                    }
                    //Erzeuge Well Tile
                    if (randomTile > 90) {
                        map[newCoordinateY][newCoordinateX] = new Well();
                    }

                    //NÄCHSTER REKURSIONSSCHRITT, wenn ein angrenzendes Feld FREI ist
                    if (generateRecursiveMap(newCoordinateX, newCoordinateY)) {
                        return true;
                    }
                }
            }
        }
        return false;   //Rekursionsabbruch, falls Goal bereits erreicht wurde
    }

    /**
     * Füllt beim Abbruch der rekursiven Kartenerzeugung die Null Felder mit Wall Objekten
     */
    public void fillNullTilesWithWalls() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (!(map[i][j] instanceof Battle
                    | map[i][j] instanceof Empty
                    | map[i][j] instanceof PlayerTile
                    | map[i][j] instanceof Wall
                    | map[i][j] instanceof Well
                    | map[i][j] instanceof Goal)) {
                    map[i][j] = new Wall();
                }
            }
        }
    }
}

//EndClassGeneratedMap
