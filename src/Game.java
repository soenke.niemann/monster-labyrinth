// BeginClassGame
/**
* Die Klasse RPG ist das eigentliche "Spiel" sie liest die Aufrufparameter aus
* , erstellt die Objekte und startet das Spiel.
*
* @author Soenke Niemann 4842491 Gruppe 12b
* @author Jasper Klinkig 4805599 Gruppe 12b
*/

public class Game {

   /**
    * Diese Methode liest die Aufrufparameter aus und wandelt sie in Integer um.
    * Außerdem wird überprüft, ob genug Aufrufparameter vorhanden sind, ansonsten
    * gibt es eine Fehlermeldung und das Spiel wird beendet.
    * @param args Der String mit den Aufrufparametern.
    */
    public static void main(String[] args) {

        //Soll vor Spielbeginn ein Tutorial gezeigt werden?
        final boolean tutorial = true;

        //Testen, ob alle args vom Typ Integer sind, dann ist abgleich:=args, sonst abgleich<args
        int abgleich = 0;
        for (int i = 0; i < args.length; i++) {
            java.util.Scanner scannerArgs = new java.util.Scanner(args[i]);
            if (scannerArgs.hasNextInt()) {
                abgleich++;
            }
            scannerArgs.close();
        }

        //------------------------TESTEN DER AUFRUFPARAMETER------------------------
        //Teste ob 3 Integer Aufrufparameter vorhanden, dann testet ob diese im richtigen Bereich liegen
        if ((args.length == 3) && (abgleich == args.length) && (Integer.parseInt(args[0]) > 0)
            && (Integer.parseInt(args[1]) > 0) && (Integer.parseInt(args[2]) > 0)
            && (Integer.parseInt(args[2]) < 100)) {


            //------------------------TESTS BESTANDEN, SPIEL KANN BEGINNEN----------------------
            //TUTORIAL
            if (tutorial) {
                System.out.println("------------------------------------------------------------------");
                System.out.println("THE \"Player moves through a labyrinth and defeats Monsters\" GAME");
                System.out.println("2017 Edition - by Jasper Klinkig and Sönke Niemann");
                System.out.println("------------------------------------------------------------------");
                System.out.println();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("HOW TO PLAY:");
                System.out.println("Your OBJECTIVE is to find your way through the dangerous field");
                System.out.println("and reach your Goal \"G\" ALIVE!");
                System.out.println("BUT... Make sure to pay attention to the BATTLEGROUNDS \"B\"!");
                System.out.println("There are mighty scary and terrifying MONSTERS waiting for you....");
                System.out.println("Your only way to come out as a hero is to defeat the monsters.");
                System.out.println();

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("If you find yourself badly injured from a tough fight");
                System.out.println("then move forward to a WELL \"O\". They heal 50 HP.");
                System.out.println();
                System.out.println("You start at Position \"P\"!");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            //-------------------------------------------------------------------------------------------------
            //ABFRAGEN EINIGER PARAMETER zur Karte, Name des Spielers
            //-------------------------------------------------------------------------------------------------

            //Definition des Scanners für alle weiteren Eingaben
            java.util.Scanner scannerParameter = new java.util.Scanner(System.in);

            //SCANNER für PlayerName
            System.out.println();
            System.out.println("--> What's your name?");
            String readName = "";
            while (readName.equals("")) {
                readName = scannerParameter.nextLine();
            }

            //Erstelle neues Spieler-Objekt mit dem eingelesenen Namen
            Player player1 = new Player(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
                Integer.parseInt(args[2]), readName);

            //SCANNER für Kartengröße
            int readHeight = 0;
            int readWidth = 0;
            //java.util.Scanner scannerParameter = new java.util.Scanner(System.in);
            System.out.println("--> Please define the map's height (recommendend: min 5, max 50) !");
            while (!scannerParameter.hasNextInt()) {
                scannerParameter.next();
            }
            readHeight = scannerParameter.nextInt();
            System.out.println("--> Please define the map's width (recommendend: min 5, max 50) !");
            while (!scannerParameter.hasNextInt()) {
                scannerParameter.next();
            }
            readWidth = scannerParameter.nextInt();

            //SCANNER ob Map mit farbigen Symbolen gezeichnet werden soll
            boolean colouredMap = false;
            System.out.println("--> Should the map be coloured? (type \"yes\" or \"no\")");
            //java.util.Scanner scannerParameter = new java.util.Scanner(System.in);
            String readYesNo = "";
            while (!readYesNo.equals("yes") && !readYesNo.equals("no")) {
                readYesNo = scannerParameter.nextLine();
                if (readYesNo.equals("yes")) {
                    colouredMap = true;
                }
                if (readYesNo.equals("no")) {
                    colouredMap = false;
                }
            }


            //-------------------------------------------------------------------------------------------------
            //MAP-PRINTING und Start des Spielablaufs MIT RANDOM MAP-GENERATOR
            //-------------------------------------------------------------------------------------------------

            System.out.println();
            System.out.println("Good luck, " + player1.getName() + "! Map is being generated...");
            System.out.println("------------------------------------------------------------------");
            System.out.println();

            //Erzeuge neues Map-Objekt          (width,   height,   player,   Farbe ja nein)
            GeneratedMap map1 = new GeneratedMap(readWidth, readHeight, player1, colouredMap);
            map1.createNullMap();
            map1.createOuterWalls();
            map1.plantPlayerAndGoal();

            map1.generateRecursiveMap(map1.playerXCoordinate, map1.playerYCoordinate);
            map1.fillNullTilesWithWalls();

            map1.start();

        } else {
            //------------------------FALSCHE PARAMETER, BEENDEN------------------------
            System.out.println("Error! Wrong parameter values. Program is being closed!");
            System.exit(0);
        }
    }
}
// EndClassGame
