# Monster Labyrinth
by Jasper Klinkig and Sönke Niemann

A game in which you spawn in a labyrinth of Walls ('#'),
trying to find a way to the Goal ('G').
Along your way you encounter monsters of different kinds and random strength 
at Battlegrounds('B').
Your position is marked with an 'P'.

| Monster Type | Speciality |
| ------ | ------ |
| Standard | none |
| Striker | can deal critical damage, more dangerous for your own health | 
| Shield | holds an additional shield, harder to kill | 

You have to defeat any monster in your way. To fill up your heath points,
you will find wells ('O') as you fight your way through the labyrinth.

Compile the program with `javac Game.java`.
You need to run the program with a set of parameters:

| Parameter | Example value |
| ------ | ------ |
| Health points of the player | 100 |
| Attack points of the player | 20 | 
| Hit probability of the player's attack | 80 | 

An typical run of the program after compiling would therefore look like this:
`java Game 100 20 80`